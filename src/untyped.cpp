#include "untyped.h"

Untyped::Untyped(const Untyped& other) {
    delete [] arr;
    arr = new MixedType[other.SIZE];
    for(unsigned i = 0; i < other.current_size; i++) {
        arr[i] = other.arr[i];
    }

    current_index = other.current_index;
    current_size = other.current_size; 
}

Untyped::Untyped(Untyped&& other) {
    delete [] arr;
    arr = new MixedType[other.SIZE];
    for(unsigned i = 0; i < other.current_size; i++) {
        arr[i] = other.arr[i];
    }

    current_index = other.current_index;
    current_size = other.current_size;

    delete [] other.arr;
    other.arr = NULL;
    other.current_size = other.current_index = other.SIZE = 0;
}

Untyped& Untyped::operator=(const Untyped& other) {
    if(this != &other) {
        delete [] arr;
        arr = new MixedType[other.SIZE];
        for(unsigned i = 0; i < other.current_size; i++) {
            arr[i] = other.arr[i];
        }

        current_index = other.current_index;
        current_size = other.current_size;
    }

    return *this;
}

Untyped& Untyped::operator=(Untyped&& other) {
    if(this != &other) {
        delete [] arr;
        arr = new MixedType[other.SIZE];
        for(unsigned i = 0; i < other.current_size; i++) {
            arr[i] = other.arr[i];
        }

        current_index = other.current_index;
        current_size = other.current_size;

        delete [] other.arr;
        other.arr = NULL;
        other.current_size = other.current_index = other.SIZE = 0;
    }

    return *this;
}

void Untyped::append(const short el) {
    if(current_index >= SIZE) {
        SIZE *= 2;
        resize();
    }

    arr[current_index++].short_value = el;
    arr[current_index - 1].is_short = 1;
    ++current_size;
}

void Untyped::append(const unsigned short el) {
    if(current_index >= SIZE) {
        SIZE *= 2;
        resize();
    }

    arr[current_index++].unsigned_short_value = el;
    arr[current_index - 1].is_unsigned_short = 1;
    ++current_size;
}

void Untyped::append(const int el) {
    if(current_index >= SIZE) {
        SIZE *= 2;
        resize();
    }

    arr[current_index++].int_value = el;
    arr[current_index - 1].is_int = 1;
    ++current_size;
}

void Untyped::append(const long el) {
    if(current_index >= SIZE) {
        SIZE *= 2;
        resize();
    }

    arr[current_index++].long_value = el;
    arr[current_index - 1].is_long = 1;
    ++current_size;
}

void Untyped::append(const long long el) {
    if(current_index >= SIZE) {
        SIZE *= 2;
        resize();
    }

    arr[current_index++].long_long_value = el;
    arr[current_index - 1].is_long_long = 1;
    ++current_size;
}

void Untyped::append(const unsigned el) {
    if(current_index >= SIZE) {
        SIZE *= 2;
        resize();
    }

    arr[current_index++].unsigned_value = el;
    arr[current_index - 1].is_unsigned = 1;
    ++current_size;
}

void Untyped::append(const unsigned long el) {
    if(current_index >= SIZE) {
        SIZE *= 2;
        resize();
    }

    arr[current_index++].unsigned_long_value = el;
    arr[current_index - 1].is_unsigned_long = 1;
    ++current_size;
}

void Untyped::append(const unsigned long long el) {
    if(current_index >= SIZE) {
        SIZE *= 2;
        resize();
    }

    arr[current_index++].unsigned_long_long_value = el;
    arr[current_index - 1].is_unsigned_long_long = 1;
    ++current_size;
}

void Untyped::append(const string el) {
    if(current_index >= SIZE) {
        SIZE *= 2;
        resize();
    }

    arr[current_index++].string_value = el;
    arr[current_index - 1].is_string = 1;
    ++current_size;
}

void Untyped::append(const float el) {
    if(current_index >= SIZE) {
        SIZE *= 2;
        resize();
    }

    arr[current_index++].float_value = el;
    arr[current_index - 1].is_float = 1;
    ++current_size;
}

void Untyped::append(const double el) {
    if(current_index >= SIZE) {
        SIZE *= 2;
        resize();
    }

    arr[current_index++].double_value = el;
    arr[current_index - 1].is_double = 1;
    ++current_size;
}

void Untyped::append(const char el) {
    if(current_index >= SIZE) {
        SIZE *= 2;
        resize();
    }

    arr[current_index++].char_value = el;
    arr[current_index - 1].is_char = 1;
    ++current_size;
}

void Untyped::resize() {
    MixedType new_arr[SIZE];
    for(unsigned i = 0; i < SIZE; i++) {
        if(i > current_size) {
            new_arr[i].int_value = 0;
            new_arr[i].is_int = 1;
        }
        else
            new_arr[i] = arr[i];
    }

    arr = new MixedType[SIZE];
    for(unsigned i = 0; i < SIZE; i++)
        arr[i] = new_arr[i];
}

string Untyped::operator[](const unsigned int index) const {
    if(index >= SIZE)
        throw "Index out of bound";

    if(arr[index].is_string)
        return arr[index].string_value;
    else if(arr[index].is_short)
        return to_string(arr[index].short_value);
    else if(arr[index].is_unsigned_short)
        return to_string(arr[index].unsigned_short_value);
    else if(arr[index].is_long)
        return to_string(arr[index].long_value);
    else if(arr[index].is_long_long)
        return to_string(arr[index].long_long_value);
    else if(arr[index].is_int)
        return to_string(arr[index].int_value);
    else if(arr[index].is_unsigned)
        return to_string(arr[index].unsigned_value);
    else if(arr[index].is_unsigned_long)
        return to_string(arr[index].unsigned_long_value);
    else if(arr[index].is_unsigned_long_long)
        return to_string(arr[index].unsigned_long_long_value);
    else if(arr[index].is_float)
        return to_string(arr[index].float_value);
    else if(arr[index].is_double)
        return to_string(arr[index].double_value);
    else if(arr[index].is_char)
        return string(1, arr[index].char_value);
}

string Untyped::at(const unsigned int index) const {
    return operator[](index);
}

string Untyped::type(const unsigned int index) const {
    if(index >= SIZE)
        throw "Index out of bound";
    
    if(arr[index].is_short)
        return "short";
    else if(arr[index].is_unsigned_short)
        return "unsigned short";
    else if(arr[index].is_string)
        return "string";
    else if(arr[index].is_long)
        return "long";
    else if(arr[index].is_long_long)
        return "long long";
    else if(arr[index].is_int)
        return "int";
    else if(arr[index].is_unsigned)
        return "unsigned";
    else if(arr[index].is_unsigned_long)
        return "unsigned long";
    else if(arr[index].is_unsigned_long_long)
        return "unsigned long long";
    else if(arr[index].is_float)
        return "float";
    else if(arr[index].is_double)
        return "double";
    else if(arr[index].is_char)
        return "char";
}

bool Untyped::operator==(const Untyped& other) {
    if(this == &other)
        return true;

    if(current_size != other.current_size)
        return false;

    for(unsigned int i = 0; i < current_size; i++) {
        if(arr[i] != other.arr[i])
            return false;
    }

    return true;
}

bool Untyped::operator!=(const Untyped& u) {
    return !operator==(u);
}

void Untyped::reorder(const vector<string> & by_types) {
    MixedType new_arr[SIZE];

    unsigned j = 0;
    for(auto type_ : by_types) {
        for(unsigned i = 0; i < current_size; i++)
            if(type(i) == type_) new_arr[j++] = arr[i];
    }

    for(unsigned i = 0; i < current_size; i++)
        arr[i] = new_arr[i];
}

void Untyped::reorder(vector<string> && by_types) {
    reorder(by_types);
}

void Untyped::set(unsigned int index, const short value) {
    if(index >= SIZE) {
        SIZE = index * 2;
        resize();

    }

    if(index > current_size)
        current_size = index;

    arr[index].is_unsigned_short =
    arr[index].is_int = arr[index].is_long = arr[index].is_long_long =
    arr[index].is_unsigned = arr[index].is_unsigned_long = arr[index].is_unsigned_long_long =
    arr[index].is_float = arr[index].is_double =
    arr[index].is_char = arr[index].is_string = false;

    arr[index].is_short = true;
    arr[index].short_value = value;
}

void Untyped::set(const unsigned int index, const unsigned short value) {
    if(index >= SIZE) {
        SIZE = index * 2;
        resize();

    }

    if(index > current_size)
        current_size = index;

    arr[index].is_short =
    arr[index].is_int = arr[index].is_long = arr[index].is_long_long =
    arr[index].is_unsigned = arr[index].is_unsigned_long = arr[index].is_unsigned_long_long =
    arr[index].is_float = arr[index].is_double =
    arr[index].is_char = arr[index].is_string = false;

    arr[index].is_unsigned_short = true;
    arr[index].unsigned_short_value = value;
}

void Untyped::set(const unsigned int index, const int value) {
    if(index >= SIZE) {
        SIZE = index * 2;
        resize();
    }

    if(index > current_size)
        current_size = index;
    
    arr[index].is_short = arr[index].is_unsigned_short =
    arr[index].is_long = arr[index].is_long_long =
    arr[index].is_unsigned = arr[index].is_unsigned_long = arr[index].is_unsigned_long_long =
    arr[index].is_float = arr[index].is_double =
    arr[index].is_char = arr[index].is_string = false;

    arr[index].is_int = true;
    arr[index].int_value = value;
}

void Untyped::set(const unsigned int index, const long value) {
    if(index >= SIZE) {
        SIZE = index * 2;
        resize();

    }

    if(index > current_size)
        current_size = index;

    arr[index].is_short = arr[index].is_unsigned_short =
    arr[index].is_int = arr[index].is_long_long =
    arr[index].is_unsigned = arr[index].is_unsigned_long = arr[index].is_unsigned_long_long = 
    arr[index].is_float = arr[index].is_double =
    arr[index].is_char = arr[index].is_string = false;

    arr[index].is_long = true;
    arr[index].long_value = value;
}

void Untyped::set(const unsigned int index, const long long value) {
    if(index >= SIZE) {
        SIZE = index * 2;
        resize();

    }

    if(index > current_size)
        current_size = index;

    arr[index].is_short = arr[index].is_unsigned_short =
    arr[index].is_int = arr[index].is_long =
    arr[index].is_unsigned = arr[index].is_unsigned_long = arr[index].is_unsigned_long_long =
    arr[index].is_float = arr[index].is_double =
    arr[index].is_char = arr[index].is_string = false;

    arr[index].is_long_long = true;
    arr[index].long_long_value = value;
}

void Untyped::set(const unsigned int index, const unsigned value) {
    if(index >= SIZE) {
        SIZE = index * 2;
        resize();
    }

    if(index > current_size)
        current_size = index;

    arr[index].is_short = arr[index].is_unsigned_short =
    arr[index].is_int = arr[index].is_long = arr[index].is_long_long =
    arr[index].is_unsigned_long = arr[index].is_unsigned_long_long =
    arr[index].is_float = arr[index].is_double =
    arr[index].is_char = arr[index].is_string = false;

    arr[index].is_unsigned = true;
    arr[index].unsigned_value = value;
}


void Untyped::set(const unsigned int index, const unsigned long value) {
    if(index >= SIZE) {
        SIZE = index * 2;
        resize();
    }

    if(index > current_size)
        current_size = index;

    arr[index].is_short = arr[index].is_unsigned_short =
    arr[index].is_int = arr[index].is_long = arr[index].is_long_long =
    arr[index].is_unsigned = arr[index].is_unsigned_long_long =
    arr[index].is_float = arr[index].is_double =
    arr[index].is_char = arr[index].is_string = false;

    arr[index].is_unsigned_long = true;
    arr[index].unsigned_long_value = value;
}

void Untyped::set(const unsigned int index, const unsigned long long value) {
    if(index >= SIZE) {
        SIZE = index * 2;
        resize();
    }

    if(index > current_size)
        current_size = index;

    arr[index].is_short = arr[index].is_unsigned_short =
    arr[index].is_int = arr[index].is_long = arr[index].is_long_long =
    arr[index].is_unsigned = arr[index].is_unsigned_long =
    arr[index].is_float = arr[index].is_double =
    arr[index].is_char = arr[index].is_string = false;

    arr[index].is_unsigned_long_long = true;
    arr[index].unsigned_long_long_value = value;
}

void Untyped::set(const unsigned int index, const float value) {
    if(index >= SIZE) {
        SIZE = index * 2;
        resize();
    }

    if(index > current_size)
        current_size = index;

    arr[index].is_short = arr[index].is_unsigned_short =
    arr[index].is_int = arr[index].is_long = arr[index].is_long_long =
    arr[index].is_unsigned = arr[index].is_unsigned_long = arr[index].is_unsigned_long_long =
    arr[index].is_double =
    arr[index].is_char = arr[index].is_string = false;

    arr[index].is_float = true;
    arr[index].float_value = value;
}

void Untyped::set(const unsigned int index, const double value) {
    if(index >= SIZE) {
        SIZE = index * 2;
        resize();
    }

    if(index > current_size)
        current_size = index;

    arr[index].is_short = arr[index].is_unsigned_short =
    arr[index].is_int = arr[index].is_long = arr[index].is_long_long =
    arr[index].is_unsigned = arr[index].is_unsigned_long = arr[index].is_unsigned_long_long =
    arr[index].is_float =
    arr[index].is_char = arr[index].is_string = false;

    arr[index].is_double = true;
    arr[index].double_value = value;
}

void Untyped::set(const unsigned int index, const string value) {
    if(index >= SIZE) {
        SIZE = index * 2;
        resize();
    }


    if(index > current_size)
        current_size = index;

    arr[index].is_short = arr[index].is_unsigned_short =
    arr[index].is_int = arr[index].is_long = arr[index].is_long_long =
    arr[index].is_unsigned = arr[index].is_unsigned_long = arr[index].is_unsigned_long_long =
    arr[index].is_float = arr[index].is_double =
    arr[index].is_char = false;

    arr[index].is_string = true;
    arr[index].string_value = value;
}

void Untyped::set(const unsigned int index, const char value) {
    if(index >= SIZE) {
        SIZE *= 2;
        resize();
    }

    if(index > current_size)
        current_size = index;

    arr[index].is_short = arr[index].is_unsigned_short =
    arr[index].is_int = arr[index].is_long = arr[index].is_long_long =
    arr[index].is_unsigned = arr[index].is_unsigned_long = arr[index].is_unsigned_long_long =
    arr[index].is_float = arr[index].is_double =
    arr[index].is_string = false;

    arr[index].is_char = true;
    arr[index].char_value = value;
}

void Untyped::extend(const Untyped& u) {
    if(SIZE < const_cast<Untyped&>(u).size() || SIZE < current_size + const_cast<Untyped&>(u).size()) {
        SIZE = current_size + const_cast<Untyped&>(u).size() * 2;
        resize();
    }

    current_size += const_cast<Untyped&>(u).size();
    for(unsigned i = 0; i < const_cast<Untyped&>(u).size(); i++) {
        arr[current_index++] = const_cast<Untyped&>(u).arr[i];
    }
}

void Untyped::extend(Untyped&& u) {
    extend(u);
}

bool Untyped::member(const short val) const {
    for(unsigned i = 0; i < current_size; ++i)
        if(arr[i].is_short && arr[i].short_value == val)
            return true;

    return false;
}

bool Untyped::member(const unsigned short val) const {
    for(unsigned i = 0; i < current_size; ++i)
        if(arr[i].is_unsigned_short && arr[i].unsigned_short_value == val)
            return true;

    return false;
}

bool Untyped::member(const int val) const {
    for(unsigned i = 0; i < current_size + 1; ++i)
        if(arr[i].is_int && arr[i].int_value == val)
            return true;

    return false;
}

bool Untyped::member(const long val) const {
    for(unsigned i = 0; i < current_size; ++i)
        if(arr[i].is_long && arr[i].long_value == val)
            return true;

    return false;
}

bool Untyped::member(const long long val) const {
    for(unsigned i = 0; i < current_size; ++i)
        if(arr[i].is_long_long && arr[i].long_long_value == val)
            return true;

    return false;
}
   
bool Untyped::member(const unsigned val) const {
    for(unsigned i = 0; i < current_size; ++i)
        if(arr[i].is_unsigned && arr[i].unsigned_value == val)
            return true;

    return false;
}
    
bool Untyped::member(const unsigned long val) const {
    for(unsigned i = 0; i < current_size; ++i)
        if(arr[i].is_unsigned_long && arr[i].unsigned_long_value == val)
            return true;

    return false;
}

bool Untyped::member(const unsigned long long val) const {
    for(unsigned i = 0; i < current_size; ++i)
        if(arr[i].is_unsigned_long_long && arr[i].unsigned_long_long_value == val)
            return true;

    return false;
}

bool Untyped::member(const float val) const {
    for(unsigned i = 0; i < current_size; ++i)
        if(arr[i].is_float && arr[i].float_value == val)
            return true;

    return false;
}

bool Untyped::member(const double val) const {
    for(unsigned i = 0; i < current_size; ++i)
        if(arr[i].is_double && arr[i].double_value == val)
            return true;

    return false;
}
    
bool Untyped::member(const string & val) const {
    for(unsigned i = 0; i < current_size; ++i)
        if(arr[i].is_string && arr[i].string_value == val)
            return true;

    return false;
}

bool Untyped::member(const char val) const {
    for(unsigned i = 0; i < current_size; ++i)
        if(arr[i].is_char && arr[i].char_value == val)
            return true;

    return false;
}

Untyped & Untyped::operator+=(const Untyped& u) {
    extend(u);

    return *this;
}

Untyped Untyped::operator-(const Untyped& u) {
    Untyped new_u;

    for(unsigned i = 0; i < current_size; i++) {
        if(arr[i].is_short && !const_cast<Untyped&>(u).member(arr[i].short_value))
            new_u.append(arr[i].short_value);
        else if(arr[i].is_unsigned_short && !const_cast<Untyped&>(u).member(arr[i].unsigned_short_value))
            new_u.append(arr[i].unsigned_short_value);
        else if(arr[i].is_int && !const_cast<Untyped&>(u).member(arr[i].int_value))
            new_u.append(arr[i].int_value);
        else if(arr[i].is_long && !const_cast<Untyped&>(u).member(arr[i].long_value))
            new_u.append(arr[i].long_value);
        else if(arr[i].is_long_long && !const_cast<Untyped&>(u).member(arr[i].long_long_value))
            new_u.append(arr[i].long_long_value);
        else if(arr[i].is_unsigned && !const_cast<Untyped&>(u).member(arr[i].unsigned_value))
            new_u.append(arr[i].unsigned_value);
        else if(arr[i].is_unsigned_long && !const_cast<Untyped&>(u).member(arr[i].unsigned_long_value))
            new_u.append(arr[i].unsigned_long_value);
        else if(arr[i].is_unsigned_long_long && !const_cast<Untyped&>(u).member(arr[i].unsigned_long_long_value))
            new_u.append(arr[i].unsigned_long_long_value);
        else if(arr[i].is_string && !const_cast<Untyped&>(u).member(arr[i].string_value))
            new_u.append(arr[i].string_value);
        else if(arr[i].is_char && !const_cast<Untyped&>(u).member(arr[i].char_value))
            new_u.append(arr[i].char_value);
        else if(arr[i].is_float && !const_cast<Untyped&>(u).member(arr[i].float_value))
            new_u.append(arr[i].float_value);
        else if(arr[i].is_double && !const_cast<Untyped&>(u).member(arr[i].double_value))
            new_u.append(arr[i].double_value);
    }

    return new_u;
}

std::ostream& operator<<(std::ostream& os, const Untyped& u) {

    os << "[";
    unsigned int size = const_cast<Untyped&>(u).size();
    for(unsigned i = 0; i < size; i++) {
        os << const_cast<Untyped&>(u)[i];
        if(i < size - 1)
            os << ", ";
    }

    os << "]\n";

    return os;
}

void Untyped::filter(vector<short> & vs) {
    for(unsigned i = 0; i < current_size; i++)
        if(arr[i].is_short) vs.push_back(arr[i].short_value);
}

void Untyped::filter(vector<unsigned short> & vs) {
    for(unsigned i = 0; i < current_size; i++)
        if(arr[i].is_unsigned_short) vs.push_back(arr[i].unsigned_short_value);
}

void Untyped::filter(vector<int> & vi) {
    for(unsigned i = 0; i < current_size; i++)
        if(arr[i].is_int) vi.push_back(arr[i].int_value);
}

void Untyped::filter(vector<long> & vl) {
    for(unsigned i = 0; i < current_size; i++)
        if(arr[i].is_long) vl.push_back(arr[i].long_value);
}

void Untyped::filter(vector<long long> & vll) {
    for(unsigned i = 0; i < current_size; i++)
        if(arr[i].is_long_long) vll.push_back(arr[i].long_long_value);
}

void Untyped::filter(vector<unsigned> & vu) {
    for(unsigned i = 0; i < current_size; i++)
        if(arr[i].is_unsigned) vu.push_back(arr[i].unsigned_value);
}
    
void Untyped::filter(vector<unsigned long> & vl) {
    for(unsigned i = 0; i < current_size; i++)
        if(arr[i].is_unsigned_long) vl.push_back(arr[i].unsigned_long_value);
}

void Untyped::filter(vector<unsigned long long> & vull) {
    for(unsigned i = 0; i < current_size; i++)
        if(arr[i].is_unsigned_long_long) vull.push_back(arr[i].unsigned_long_long_value);
}
    
void Untyped::filter(vector<float> & vf) {
    for(unsigned i = 0; i < current_size; i++)
        if(arr[i].is_float) vf.push_back(arr[i].float_value);
}
    
void Untyped::filter(vector<double> & vd) {
    for(unsigned i = 0; i < current_size; i++)
        if(arr[i].is_double) vd.push_back(arr[i].double_value);
}

void Untyped::filter(vector<string> & vs) {
    for(unsigned i = 0; i < current_size; i++)
        if(arr[i].is_string) vs.push_back(arr[i].string_value);
}

void Untyped::filter(vector<char> & vc) {
    for(unsigned i = 0; i < current_size; i++)
        if(arr[i].is_char) vc.push_back(arr[i].char_value);
}

void Untyped::shrink() {
    SIZE = current_size + 1;
    resize();
}

map<string, map<string, unsigned> > Untyped::value_types() {
    map<string, map<string, unsigned> > result;

    map<string, unsigned> current;
    for(unsigned i = 0; i < current_size; i++) {
        if(result.count(operator[](i)) && result[operator[](i)].count(type(i)))
            result[operator[](i)][type(i)]++;
        else if(result.count(operator[](i)))
            result[operator[](i)][type(i)] = 1;
        else {
            current[type(i)] = 1;
            result[operator[](i)] = current;
            current.clear();
        }
    }

    return result;
}