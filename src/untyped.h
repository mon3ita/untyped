#ifndef UNTYPED_H
#define UNTYPED_H

#include <iostream>
#include <string>
#include <vector>
#include <map>

const unsigned int START_SIZE = 20;

using std::string;
using std::to_string;
using std::vector;
using std::map;

class Untyped {
public:
    Untyped(unsigned int size = 0) : current_index(0), current_size(0) {
        if(size > 0) SIZE = size;
        else SIZE = START_SIZE;
        arr = new MixedType[SIZE];
    }

    virtual ~Untyped() {
        delete [] arr;
        arr = NULL;
        current_size = current_index = SIZE = 0;
    }

    Untyped(const Untyped&);
    Untyped(Untyped&&);

    Untyped& operator=(const Untyped&);
    Untyped& operator=(Untyped&&);

    string operator[](const unsigned int) const;

    string at(const unsigned int) const;

    string type(const unsigned int) const;

    void append(const short);
    void append(const unsigned short);
    void append(const int);
    void append(const long);
    void append(const long long);
    void append(const unsigned int);
    void append(const unsigned long int);
    void append(const unsigned long long);
    void append(const float);
    void append(const double);
    void append(const string);
    void append(const char);

    void extend(const Untyped&);
    void extend(Untyped&&);

    void set(const unsigned int, const short);
    void set(const unsigned int, const unsigned short);
    void set(const unsigned int, const int);
    void set(const unsigned int, const long);
    void set(const unsigned int, const long long);
    void set(const unsigned int, const unsigned);
    void set(const unsigned int, const unsigned long);
    void set(const unsigned int, const unsigned long long);
    void set(const unsigned int, const float);
    void set(const unsigned int, const double);
    void set(const unsigned int, const char);
    void set(const unsigned int, const string);

    unsigned int size() const { return current_size; }
    unsigned int actual_size() const { return SIZE; }

    bool member(const short) const;
    bool member(const unsigned short) const;
    bool member(const int) const;
    bool member(const long) const;
    bool member(const long long) const;
    bool member(const unsigned) const;
    bool member(const unsigned long)  const;
    bool member(const unsigned long long) const;
    bool member(const float) const;
    bool member(const double) const;
    bool member(const string &) const;
    bool member(const char) const;

    bool operator==(const Untyped&);
    bool operator!=(const Untyped&);

    void reorder(const vector<string>&);
    void reorder(vector<string>&&);

    Untyped & operator+(const Untyped&);
    Untyped & operator+=(const Untyped&);
    Untyped operator-(const Untyped&);
    Untyped & operator-=(const Untyped&);

    void filter(vector<short> &);
    void filter(vector<unsigned short>&);
    void filter(vector<int> &);
    void filter(vector<long> &);
    void filter(vector<long long>&);
    void filter(vector<unsigned> &);
    void filter(vector<unsigned long> &);
    void filter(vector<unsigned long long>&);
    void filter(vector<float> &);
    void filter(vector<double> &);
    void filter(vector<string> &);
    void filter(vector<char> &);

    void shrink();

    map<string, map<string, unsigned> > value_types();
private:
    unsigned int SIZE;
    unsigned int current_index;
    unsigned int current_size;

    struct MixedType {
        MixedType() : is_short(false), is_unsigned_short(false),
                        is_int(false), is_long(false), is_long_long(false),
                        is_unsigned(false), is_unsigned_long(false), is_unsigned_long_long(false),
                        is_float(false), is_double(false), 
                        is_char(false), is_string(false) {}

        MixedType & operator=(const MixedType & m) {
            if(m.is_short) {
                is_unsigned_short = 
                    is_int = is_long = is_long_long =
                    is_unsigned = is_unsigned_long = is_unsigned_long_long =
                    is_float = is_double =
                    is_char = is_string = false;

                is_short = 1;
                short_value = m.short_value;
            } else if(m.is_unsigned_short) {
                is_short = 
                    is_int = is_long = is_long_long =
                    is_unsigned = is_unsigned_long = is_unsigned_long_long =
                    is_float = is_double =
                    is_char = is_string = false;
                is_unsigned_short = 1;
                unsigned_short_value = m.unsigned_short_value;
            } else if(m.is_int) {
                is_short = is_unsigned_short = 
                    is_long = is_long_long =
                    is_unsigned = is_unsigned_long = is_unsigned_long_long =
                    is_float = is_double =
                    is_char = is_string = false;
                is_int = 1;
                int_value = m.int_value;
            } else if(m.is_long) {
                is_short = is_unsigned_short = 
                    is_int = is_long_long =
                    is_unsigned = is_unsigned_long = is_unsigned_long_long =
                    is_float = is_double =
                    is_char = is_string = false;
                is_long = 1;
                long_value = m.long_value;
            } else if(m.is_long_long) {
                is_short = is_unsigned_short = is_int = is_long =
                    is_unsigned = is_unsigned_long = is_unsigned_long_long =
                    is_float = is_double =
                    is_char = is_string = false;
                is_long_long = 1;
                long_long_value = m.long_long_value;
            } else if(m.is_unsigned) {
                is_short = is_unsigned_short = 
                    is_int = is_long = is_long_long =
                    is_unsigned_long = is_unsigned_long_long =
                    is_float = is_double =
                    is_char = is_string = false;
                is_unsigned = 1;
                unsigned_value = m.unsigned_value;
            } else if(m.is_unsigned_long) {
                is_short = is_unsigned_short = 
                    is_int = is_long = is_long_long =
                    is_unsigned = is_unsigned_long_long =
                    is_float = is_double =
                    is_char = is_string = false;
                is_unsigned_long = 1;
                unsigned_long_value = m.unsigned_long_value;
            } else if(m.is_unsigned_long_long) {
                is_short = is_unsigned_short = 
                    is_int = is_long = is_long_long =
                    is_unsigned = is_unsigned_long =
                    is_float = is_double =
                    is_char = is_string = false;
                is_unsigned_long_long = 1;
                unsigned_long_long_value = m.unsigned_long_long_value;
            } else if(m.is_float) {
                is_short = is_unsigned_short = 
                    is_int = is_long = is_long_long =
                    is_unsigned = is_unsigned_long = is_unsigned_long_long =
                    is_double =
                    is_char = is_string = false;
                is_float = 1;
                float_value = m.float_value;
            } else if(m.is_double) {
                is_short = is_unsigned_short = 
                    is_int = is_long = is_long_long =
                    is_unsigned = is_unsigned_long = is_unsigned_long_long =
                    is_float =
                    is_char = is_string = false;
                is_double = 1;
                double_value = m.double_value;
            } else if(m.is_string) {
                is_short = is_unsigned_short = 
                    is_int = is_long = is_long_long =
                    is_unsigned = is_unsigned_long = is_unsigned_long_long =
                    is_float = is_double =
                    is_char = false;
                is_string = 1;
                string_value = m.string_value;
            } else if(m.is_char) {
                is_short = is_unsigned_short = 
                    is_int = is_long = is_long_long =
                    is_unsigned = is_unsigned_long = is_unsigned_long_long =
                    is_float = is_double =
                    is_string = false;
                is_char = 1;
                char_value = m.char_value;
            }

            return *this;
        }

        bool operator!=(const MixedType & m) {
            return ((is_int && !m.is_int) ||
                (is_long && !m.is_long) ||
                (is_long_long && !m.is_long_long) ||
                (is_short && !m.is_short) ||
                (is_unsigned_short && !m.is_unsigned_short) ||
                (is_char && !m.is_char) ||
                (is_unsigned && !m.is_unsigned) ||
                (is_unsigned_long && !m.is_unsigned_long) ||
                (is_unsigned_long_long && !m.is_unsigned_long_long) ||
                (is_float && !m.is_float) ||
                (is_double && !m.is_double) ||
                (is_string && !m.is_string) ||
                (is_int && int_value != m.int_value) ||
                (is_long && long_value != m.long_value) ||
                (is_long && long_long_value != m.long_long_value) ||
                (is_short && short_value != m.short_value) ||
                (is_unsigned_short && unsigned_short_value != m.unsigned_short_value) ||
                (is_unsigned && unsigned_value != m.unsigned_value) ||
                (is_unsigned_long && unsigned_long_value != m.unsigned_long_value) ||
                (is_unsigned_short && unsigned_long_value != m.unsigned_long_long_value) ||
                (is_float && float_value != m.float_value) ||
                (is_double && double_value != m.double_value) ||
                (is_char && char_value != m.char_value) ||
                (is_string && string_value != m.string_value));
        }

        bool is_short, is_unsigned_short, 
            is_int, is_long, is_long_long,
            is_unsigned, is_unsigned_long, is_unsigned_long_long,
            is_float, is_double, is_string, is_char;
        
        union {
            short short_value;
            unsigned short unsigned_short_value;
            int int_value;
            long long_value;
            long long long_long_value;
            unsigned unsigned_value;
            unsigned long unsigned_long_value;
            unsigned long long unsigned_long_long_value;
            float float_value;
            double double_value;
            char char_value;
        };
        string string_value;
    };

    MixedType * arr;

    void resize();

    friend std::ostream& operator<<(std::ostream&, const Untyped&);
};

std::ostream& operator<<(std::ostream&, const Untyped&);

#endif