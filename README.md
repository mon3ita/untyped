# Untyped

![](https://i.postimg.cc/nzd2C2p7/untyped-logo.png)

Implementation of a multi-typed array, like some popular languages like `Python`, `Ruby`, and others.

## METHODS

*Adds a new element at the end of the array.*

```c++
>>> append(const short);
>>> append(const unsigned short);
>>> append(const int);
>>> append(const long);
>>> append(const unsigned);
>>> append(const unsigned long);
>>> append(const unsigned long long);
>>> append(const float);
>>> append(const double);
>>> append(const char);
>>> append(const string);
```

*Sets a new value at given index of the array. If index > used size all indecies < index will be set to 0.*

```c++
>>> set(const unsigned int, const short);
>>> set(const unsigned int, const unsigned long);
>>> set(const unsigned int, const int);
>>> set(const unsigned int, const long);
>>> set(const unsigned int, const unsigned);
>>> set(const unsigned int, const unsigned long);
>>> set(const unsigned long, const unsigned long long);
>>> set(const unsigned int, const float);
>>> set(const unsigned int, const double);
>>> set(const unsigned int, const char);
>>> set(const unsigned int, const string);
```

*Checks if a value is from the array.*

```c++
>>> member(const short) const;
>>> member(const unsigned short) const;
>>> member(const int) const;
>>> member(const long) const;
>>> member(const unsigned) const;
>>> member(const unsigned long) const;
>>> member(const unsigned long long) const;
>>> member(const float) const;
>>> member(const double) const;
>>> member(const char) const;
>>> member(const string&) const;
```

*Some operators.*

```c++
>>> operator==(const Untyped &); //compares this object's elements and given as argument Untyped object
>>> operator!=(const Untyped &); // //compares this object's elements and given as argument Untyped object
>>> operator+(const Untyped&); // creates a new Untyped object, and adds all elements from this object and given as argument Untyped object
>>> operator+=(const Untyped&); //adds new elements to the original array, alias for extend
>>> operator-(const Untyped&); //finds the difference, keeps elements in a new Untyped object
>>> operator-=(const Untyped&); //finds the difference, modifies the original object
```

*Returns used size, and actual size used from the array.*

```c++
>>> size(); //used size from the array
>>> actual_size(); //allocated memoery for the array
```

*Returns the element at given index, converted to string. Throws an exception if the index is bigger than the current maximum size of the array.*

```c++
>>> operator[](const unsigned int); //returns string representation of the element at index
>>> at(const unsigned int); // same as operator[]
```

*Returns the type of the element at given index.*

```c++
>>> type(const unsigned int); //returns the name of the type of element at given index
```

*Extends the array with another.*

```c++
>>> extend(const Untyped&);
>>> extend(Untyped&&);
```

*Reorders the array by given types in a vector.*

```c++
>>> reorder(vector<string>&);
>>> reorder(vector<string>&&);
```

*Adds all values from the array into the given vector, with given type.*

```c++
>>> filter(vector<short>&);
>>> filter(vector<unsigned short>&);
>>> filter(vector<int>&);
>>> filter(vector<long>&);
>>> filter(vector<unsigned>&);
>>> filter(vector<unsigned long>&);
>>> filter(vector<unsigned long long>&);
>>> filter(vector<float>&);
>>> filter(vector<double>&);
>>> filter(vector<char>&);
>>> filter(vector<string>&);
```

*Returns all information about the elements of the array - its value, types, and number of occurrences in the array.*

```c++
>>> value_types();
```

*Resizes the array to current number of elements.*
```c++
>>> shrink();
```

## USAGE

```c++
//#include "untyped/src/untyped.h"
//   or
//#include "untyped.h"

#include <iostream>

using namespace std;

Untyped u;
u.append(1);
u.append(7.8f);
u.append("hello, world");
u.append('a');

cout << u << endl;
//[1, 7.8000, "hello, world", 'a']

cout << u[0].type << endl;
//int

Untyped copy(u);

cout << (copy == u) << endl;
//1

cout << u.member(1) << endl;
//1

//g++ path/untyped.h path/untyped.cpp file_name.cpp -o file -std=c++11
//>>> file
```

## LICENSE

GNU 3.0
